# Aplicación demostrativa de la funcionalidad Picture in Picture


[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Este widget fue desarrollado por el Ing. Jose Antonio Márquez Elguea para demostrar el funcionamiento de la funcionalidad Picture in Picture (PiP). El funcionamiento del mismo es meramente demostrativo. 

## Privilegios, permisos, politicas

+ <tizen:privilege name="http://tizen.org/privilege/tv.audio"/>
+ <tizen:privilege name="http://tizen.org/privilege/tv.channel"/>
+ <tizen:privilege name="http://tizen.org/privilege/tv.window"/>

## Documentación oficial

+ Documentacion, SDK e IDE [Samsung D forum](http://samsungdforum.com/b2b).
+ Documentacion, SDK e IDE [Samsung Developers](https://developer.samsung.com/tv).
+ Documentacion, SDK e IDE [Tizen Org](https://www.tizen.org/).
+ Para cualquier duda referente a como usar o modificar el sistema contactar al autor.

## Licencia

[MIT license](http://opensource.org/licenses/MIT). Samsung B2B | ASET team